import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { IniciarSesionPage } from './iniciar-sesion.page';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatSnackBarModule} from '@angular/material/snack-bar';
const routes: Routes = [
  {
    path: '',
    component: IniciarSesionPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    MatSnackBarModule
  ],
  declarations: [IniciarSesionPage]
})
export class IniciarSesionPageModule { }
