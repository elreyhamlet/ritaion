import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { PedidoService } from 'src/app/services/pedido/pedido.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-resumen',
  templateUrl: './resumen.component.html',
  styleUrls: ['./resumen.component.scss'],
})
export class ResumenComponent implements OnInit {
  @Output() getEvento: EventEmitter<string>
  @Output() getEventoEditar: EventEmitter<string>;
  @Output() getEventoVaciar: EventEmitter<string>;
  public objeto_resumen: any;
  public lista_resumen: any[];
  constructor(private ws_pedido: PedidoService,
    private router: Router, private snackBar: MatSnackBar) {
    this.getEvento = new EventEmitter();
  }

  ngOnInit() {
    this.getResumen();
  }
  getInvoque(evento: string) {
    this.getEvento.emit(evento);
  }

  getResumen() {
    this.ws_pedido.getResumenPedido().subscribe((res: any) => {
      //  this.objeto_resumen = this.resumen;

      console.log(res);

    });
  }

  public resumen = {
    fruta: { fruta_id: '1', fruta_nombre: 'manzana', fruta_url: '', fruta_precio: '12.00' },
    topping: { topping_id: '1', topping_nombre: 'cafe', topping_url: '', topping_precio: '4.00' },
    precio: 100.00
  };

}
