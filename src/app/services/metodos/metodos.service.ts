import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { URL_SERVICIOS } from '../../config/config';
import { Subscription } from 'rxjs';
import { VariablesEntorno } from 'src/app/models/variablesEntorno';
@Injectable({
  providedIn: 'root'
})
export class MetodosService {
  public _loading: boolean = false;

  public flagErrorModbus: boolean = false;
  public pantallaCarga: Subscription;
  public contador: number = 0;
  public flagBotonMenu: boolean = true;


  public estaConectado: boolean = false;
  public environment: VariablesEntorno;
  public codigoLecturaModbus: number;
  public sumatotalPrecio: number = 0;
  public address_confirmacion: number;
  constructor(private http: HttpClient) {

  }

  getQuery(query: string, headers: HttpHeaders) {
    const url = `${URL_SERVICIOS}/${query}`;
    return this.http.get(url, { headers });

  }
  postQuery<T>(query: string, body: any, headers: HttpHeaders) {

    const url = `${URL_SERVICIOS}/${query}`;
    return this.http.post<T>(url, body, { headers });

  }

  postQueryJon(query: string, body: string, headers: HttpHeaders) {

    const url = `${URL_SERVICIOS}/${query}`;
    return this.http.post(url, JSON.parse(body), { headers });

  }




}
