import { Injectable } from '@angular/core';
import { UsuarioService } from '../usuario/usuario.service';
import { MetodosService } from '../metodos/metodos.service';
import {
  WS_LISTAR_PEDIDOS,
  WS_SELECCIONAR_PEDIDO,
  WS_RECOGER_RESUMEN_PEDIDO,
  WS_RECOGER_PEDIDO,
  WS_RECOGER_LISTAR_JUGO
} from '../../config/rutas';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RecogerService {
  public sessionRecoger = false;
  public qrRecoger = null;
  constructor(
    private user: UsuarioService,
    private metodos: MetodosService) { }
  precio: number;
  getListaPedidosPagados(body: any) {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.user.token}`
    });
    return this.metodos.postQuery(WS_LISTAR_PEDIDOS, body, headers);
  }

  getSeleccionarPedido(body: string) {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.user.token}`
    });
    return this.metodos.postQueryJon(WS_SELECCIONAR_PEDIDO, body, headers);
  }
  getResumenPedido() {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.user.token}`
    });
    return this.metodos.postQuery(WS_RECOGER_RESUMEN_PEDIDO, '', headers);
  }
  getRecogerPedido() {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.user.token}`
    });
    return this.metodos.postQuery(WS_RECOGER_PEDIDO, '', headers);
  }
  getListarJugos(body: any) {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.user.token}`
    });
    return this.metodos.postQuery(WS_RECOGER_LISTAR_JUGO, body, headers);
  }





}
