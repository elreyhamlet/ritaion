import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
@Injectable({
  providedIn: 'root'
})
export class MetodosNativosService {

  constructor(
    private storage: Storage,
    private social: SocialSharing
  ) { }

  guardarLocalStorage(key: string, value: any) {
    return this.storage.set(key, value);
  }
  obtenerLocalStorage(key: string) {
    return this.storage.get(key);
  }
  limpiarLocalStorage(key: string) {
    return this.storage.remove(key);
  }

  compartir() {
    return this.social;
  }
}
