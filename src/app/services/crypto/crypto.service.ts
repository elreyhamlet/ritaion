import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js';
@Injectable({
  providedIn: 'root'
})
export class CryptoService {


  REAL_KEY = '13579$#@$^@1ERFX';

  constructor() { }

  // set(keys, value){
  set(value: any) {
    const data = (typeof value == 'string') ? value.toString() : JSON.stringify(value);
    const key = CryptoJS.enc.Utf8.parse(this.REAL_KEY);
    const iv = CryptoJS.enc.Utf8.parse(this.REAL_KEY);
    const encrypted = CryptoJS.AES.encrypt(
      CryptoJS.enc.Utf8.parse(data), key,
      {
        keySize: 128 / 8,
        iv,
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.Pkcs7
      });

    return encrypted.toString();
  }


  get(value: any) {
    const key = CryptoJS.enc.Utf8.parse(this.REAL_KEY);
    const iv = CryptoJS.enc.Utf8.parse(this.REAL_KEY);
    const decrypted = CryptoJS.AES.decrypt(value, key, {
      keySize: 128 / 8,
      iv,
      mode: CryptoJS.mode.CBC,
      padding: CryptoJS.pad.Pkcs7
    });

    return decrypted.toString(CryptoJS.enc.Utf8);
  }
  getJson(value: any) {
    const key = CryptoJS.enc.Utf8.parse(this.REAL_KEY);
    const iv = CryptoJS.enc.Utf8.parse(this.REAL_KEY);
    const decrypted = CryptoJS.AES.decrypt(value, key, {
      keySize: 128 / 8,
      iv,
      mode: CryptoJS.mode.CBC,
      padding: CryptoJS.pad.Pkcs7
    });

    return JSON.parse(decrypted.toString(CryptoJS.enc.Utf8));
  }
}
