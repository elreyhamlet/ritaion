import { Injectable } from '@angular/core';
import { MetodosService } from '../metodos/metodos.service';
import { UsuarioService } from '../usuario/usuario.service';
import { HttpHeaders } from '@angular/common/http';
import { WS_UBIGEO } from '../../config/rutas';

@Injectable({
  providedIn: 'root'
})
export class UbigeoService {

  constructor(private metodo: MetodosService, private usuario: UsuarioService) { }
  listaUbigeo() {
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${this.usuario.token}`
    });
    return this.metodo.postQuery(WS_UBIGEO, '',headers);
  }
}
