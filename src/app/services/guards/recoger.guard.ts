import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { RecogerService } from '../recoger/recoger.service';

@Injectable({
  providedIn: 'root'
})
export class RecogerGuard implements CanActivate {
  canActivate() {

    const bol = this.recoger.sessionRecoger;
    if (!bol) {
      this.router.navigate(['/pages/pag-int/home']);
      return false;
    }
    return true;
  }
  constructor(private router: Router, private recoger: RecogerService) {

  }
}
