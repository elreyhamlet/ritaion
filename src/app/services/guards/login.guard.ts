import { Injectable } from '@angular/core';
import { CanActivate, Router, CanLoad } from '@angular/router';
import { UsuarioService } from '../usuario/usuario.service';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate, CanLoad {
  constructor(
    public _usuarioService: UsuarioService,
    public router: Router) { }
  async canActivate() {

    return this._usuarioService.estaLogueado().then((res: boolean) => {
      if (res) {
        return true;
      } else {
        this.router.navigate(['/login']);
        return false;
      }
    });
  }
  async canLoad() {

    return this._usuarioService.estaLogueado().then((res: boolean) => {
      if (res) {
        return true;
      } else {
        this.router.navigate(['/login']);
        return false;
      }
    });
  }
}

