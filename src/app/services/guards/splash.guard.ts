import { Injectable } from '@angular/core';
import { CanActivate, Router, CanLoad } from '@angular/router';
import { PedidoSesionService } from '../../sesion/pedido.sesion.service';
import { MetodosNativosService } from '../nativo/metodos-nativos.service';


@Injectable({
  providedIn: 'root'
})
export class SplashGuard implements CanLoad {

  async  canLoad() {
    const sesion = await this.sesion.obtenerLocalStorage('token');

    if (sesion) {
      return true;
    } else {
      if (this.splash.splashLoad) {

        return true;
      } else {
        this.router.navigate(['/login']);
      }
    }

    return this.splash.splashLoad;
  }
  constructor(
    private router: Router,
    private splash: PedidoSesionService,
    private sesion: MetodosNativosService
  ) {

  }
}
