import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { UsuarioService } from '../usuario/usuario.service';
import { ResultadoWS } from 'src/app/models/resultadoWS';

@Injectable({
  providedIn: 'root'
})
export class VerificaTokenGuard implements CanActivate {

  constructor(
    public usuarioService: UsuarioService,
    public router: Router) { }
  async  canActivate(): Promise<boolean> {

    const tok = await this.usuarioService.load();
    if (tok !== null) {
      const token = tok;
      const payload = JSON.parse(atob(token.split('.')[1]));
      const expirado = this.expirado(payload.exp);
      if (expirado) {
        this.router.navigate(['/login']);
        return false;
      }
      return this.verificaRenueva(payload.exp);
    } else {
      this.router.navigate(['/login']);
      return false;
    }

  }


  verificaRenueva(fechaExp: number): Promise<boolean> {

    return new Promise((resolve, reject) => {

      const tokenExp = new Date(fechaExp * 1000);
      const ahora = new Date();

      ahora.setTime(ahora.getTime() + (1 * 30 * 60 * 1000));
      // hora - minutos - segundos -miligenduso  
      if (tokenExp.getTime() > ahora.getTime()) {
        resolve(true);
      } else {

        this.usuarioService.renuevaToken().subscribe((res: ResultadoWS) => {
          if (res.estado) {
            const strToken = `${res.payload.access_token}`;
            const expires_in = res.payload.expires_in;
            this.usuarioService.guardarStorage(strToken);
            resolve(true);
          } else {
            reject(false);
          }
        }, () => {
          reject(false);
        });



      }

    });

  }


  expirado(fechaExp: number) {

    let ahora = new Date().getTime() / 1000;

    if (fechaExp < ahora) {
      return true;
    } else {
      return false;
    }


  }
}
