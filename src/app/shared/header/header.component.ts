import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { UsuarioService } from '../../services/usuario/usuario.service';
 

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
 
  @Input() titulo: string;
  @Input() clase: string;
  @Input() back:string = '_ion-back-blanco';
  constructor(private router: Router, private service: UsuarioService) {
 
  }

  ngOnInit() {
  }
  logout(){
    console.log('go');
    this.service.logout();
  }
  home(){
    this.router.navigate(['/pages/pag-int/home']);
  }
}


