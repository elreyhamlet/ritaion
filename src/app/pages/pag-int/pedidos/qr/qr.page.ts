import { Component, OnInit, OnDestroy } from '@angular/core';
import { PedidoService } from '../../../../services/pedido/pedido.service';
import { PedidoSesionService } from '../../../../sesion/pedido.sesion.service';
import { PedidoSocketService } from '../../../../services/pedido/pedido-socket.service';
import { Subscription } from 'rxjs';
import { MetodosNativosService } from '../../../../services/nativo/metodos-nativos.service';
import { Router } from '@angular/router';
import { CryptoService } from '../../../../services/crypto/crypto.service';

@Component({
  selector: 'app-qr',
  templateUrl: './qr.page.html',
  styleUrls: ['./qr.page.scss'],
})
export class QrPage implements OnInit, OnDestroy {
  title = 'app';
  elementType = 'url';
  value = '';
  rita$: Subscription = new Subscription();
  cargando = false;
  mensaje = '';
  constructor(
    private session: PedidoSesionService,
    private sPedido: PedidoSocketService,
    private nat: MetodosNativosService,
    private router: Router,
    private crypto: CryptoService
  ) { }

  ngOnInit() {
    this.preparar();
  }

  ionViewWillEnter() {
    this.escucharRitaDisponible();

  }
  preparar() {
    this.nat.obtenerLocalStorage('ID_TABLA').then((usuario: any) => {
      const pedidoId = this.session.qrPedido;
      const clienteId = usuario;
      const cry = this.crypto.set(`${pedidoId} % ${clienteId}%`);
      this.value = encodeURIComponent(cry) + 'abc';
    }, () => {
      this.mensaje = '';
    });
  }

  escucharRitaDisponible() {

    this.rita$ = this.sPedido.escucharRitaEstado().
      subscribe((res: any) => {
        console.log(res);
        this.cargando = true;
        this.mensaje = 'Su pedido se está procesando';
        const tipo = res.payload.tipoMensaje * 1;

        switch (tipo) {
          case 1:
            this.router.navigate(['/pages/pag-int/pedidos/mensaje-rechazar-pedido']);
            break;
          case 2:
            this.cargando = true;
            this.mensaje = res.payload.mensaje;
            break;
          case 3:
            this.router.navigate(['/pages/pag-int/pedidos/mensaje-confirmar-pedido']);
            break;
          default:
            this.router.navigate(['/pages/pag-int/pedidos/mensaje-rechazar-pedido']);
            break;
        }

      }, () => {
        this.cargando = false;
        this.router.navigate(['/pages/pag-int/pedidos/mensaje-rechazar-pedido']);
        this.mensaje = '';
      }, () => {
        this.cargando = false;
        this.mensaje = '';
      });
  }

  ionViewWillLeave() {

    this.rita$.unsubscribe();
  }

  ngOnDestroy() {

    this.rita$.unsubscribe();
  }
  home() {
    this.router.navigate(['/pages/pag-int/home']);
  }
}
