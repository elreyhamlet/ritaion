import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { MensajeConfirmarPedidoPage } from './mensaje-confirmar-pedido.page';

const routes: Routes = [
  {
    path: '',
    component: MensajeConfirmarPedidoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)

  ],
  declarations: [MensajeConfirmarPedidoPage]
})
export class MensajeConfirmarPedidoPageModule { }
