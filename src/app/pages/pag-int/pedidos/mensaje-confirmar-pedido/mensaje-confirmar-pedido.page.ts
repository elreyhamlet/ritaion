import { Component, OnInit, OnDestroy } from '@angular/core';
import { PedidoService } from '../../../../services/pedido/pedido.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-mensaje-confirmar-pedido',
  templateUrl: './mensaje-confirmar-pedido.page.html',
  styleUrls: ['./mensaje-confirmar-pedido.page.scss'],
})
export class MensajeConfirmarPedidoPage implements OnInit, OnDestroy {


  constructor(private pedido: PedidoService, private router: Router) { }


  ngOnInit() {

    this.pedido.sessionPedido = false;

  }
  ionViewWillEnter() {
    this.pedido.sessionPedido = false;
  }

  ionViewWillLeave() {
    this.pedido.sessionPedido = false;
  }
  ngOnDestroy() {
    this.pedido.sessionPedido = false;
  }


  home() {
    this.router.navigate(['/pages/pag-int/home']);
  }
}
