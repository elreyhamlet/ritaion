import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PagIntPage } from './pag-int.page';
import { VerificaTokenGuard } from '../../services/guards/verifica-token.guard';

const routes: Routes = [
  {
    path: '', component: PagIntPage, children: [
      { path: '', redirectTo: 'home' },
      { canActivate: [VerificaTokenGuard], path: 'contactanos', loadChildren: './contactanos/contactanos.module#ContactanosPageModule' },
      { canActivate: [VerificaTokenGuard], path: 'pagos', loadChildren: './pagos/pagos.module#PagosPageModule' },
      { canActivate: [VerificaTokenGuard], path: 'pedidos', loadChildren: './pedidos/pedidos.module#PedidosPageModule' },
      { canActivate: [VerificaTokenGuard], path: 'perfil', loadChildren: './perfil/perfil.module#PerfilPageModule' },
      { canActivate: [VerificaTokenGuard], path: 'recoger', loadChildren: './recoger/recoger.module#RecogerPageModule' },
      { canActivate: [VerificaTokenGuard], path: 'home', loadChildren: './home/home.module#HomePageModule' }


    ]
  },

];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class PagIntRoutingModule { }
