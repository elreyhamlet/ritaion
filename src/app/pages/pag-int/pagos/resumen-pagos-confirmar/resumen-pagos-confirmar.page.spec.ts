import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResumenPagosConfirmarPage } from './resumen-pagos-confirmar.page';

describe('ResumenPagosConfirmarPage', () => {
  let component: ResumenPagosConfirmarPage;
  let fixture: ComponentFixture<ResumenPagosConfirmarPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResumenPagosConfirmarPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResumenPagosConfirmarPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
