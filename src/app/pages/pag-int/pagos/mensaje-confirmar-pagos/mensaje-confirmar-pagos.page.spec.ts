import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MensajeConfirmarPagosPage } from './mensaje-confirmar-pagos.page';

describe('MensajeConfirmarPagosPage', () => {
  let component: MensajeConfirmarPagosPage;
  let fixture: ComponentFixture<MensajeConfirmarPagosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MensajeConfirmarPagosPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MensajeConfirmarPagosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
