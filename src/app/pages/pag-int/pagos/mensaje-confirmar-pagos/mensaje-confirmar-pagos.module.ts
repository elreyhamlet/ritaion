import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MensajeConfirmarPagosPage } from './mensaje-confirmar-pagos.page';

const routes: Routes = [
  {
    path: '',
    component: MensajeConfirmarPagosPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MensajeConfirmarPagosPage]
})
export class MensajeConfirmarPagosPageModule {}
