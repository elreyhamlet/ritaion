import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { PagarService } from '../../../../services/pago/pagar.service';

@Component({
  selector: 'app-mensaje-rechazar-pagos',
  templateUrl: './mensaje-rechazar-pagos.page.html',
  styleUrls: ['./mensaje-rechazar-pagos.page.scss'],
})
export class MensajeRechazarPagosPage implements OnInit, OnDestroy {

  constructor(private session: PagarService, private router: Router) { }

  ngOnInit() {
    this.session.sessionPagar = false;
  }
  ionViewWillEnter() {
    this.session.sessionPagar = false;
  }

  ionViewWillLeave() {
    this.session.sessionPagar = false;
  }

  ngOnDestroy() {
    this.session.sessionPagar = false;
  }

  home() {

    this.router.navigate(['/pages/pag-int/home']);
  }
}
