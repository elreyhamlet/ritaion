import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MensajeRechazarPagosPage } from './mensaje-rechazar-pagos.page';

const routes: Routes = [
  {
    path: '',
    component: MensajeRechazarPagosPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MensajeRechazarPagosPage]
})
export class MensajeRechazarPagosPageModule {}
