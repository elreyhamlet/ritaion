import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MensajeRechazarPagosPage } from './mensaje-rechazar-pagos.page';

describe('MensajeRechazarPagosPage', () => {
  let component: MensajeRechazarPagosPage;
  let fixture: ComponentFixture<MensajeRechazarPagosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MensajeRechazarPagosPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MensajeRechazarPagosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
