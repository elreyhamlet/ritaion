import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PagosPage } from './pagos.page';
import { PagarGuard } from '../../../services/guards/pagar.guard';
import { VerificaTokenGuard } from '../../../services/guards/verifica-token.guard';

const routes: Routes = [
  {
    path: '', canActivate: [VerificaTokenGuard], component: PagosPage, children: [
      { path: '', redirectTo: 'leer-codigo' },
      {
        path: 'leer-codigo',
        loadChildren: './leer-codigo/leer-codigo.module#LeerCodigoPageModule'
      },
      {
        canActivate: [PagarGuard],
        path: 'recoger-pedido',
        loadChildren: './recoger-pedido/recoger-pedido.module#RecogerPedidoPageModule'
      },
      {
        canActivate: [PagarGuard],
        path: 'datos-bancarios',
        loadChildren: './datos-bancarios/datos-bancarios.module#DatosBancariosPageModule'
      },
      {
        canActivate: [PagarGuard],
        path: 'resumen-pagos-confirmar',
        loadChildren: './resumen-pagos-confirmar/resumen-pagos-confirmar.module#ResumenPagosConfirmarPageModule'
      },
      {
        canActivate: [PagarGuard],
        path: 'mensaje-confirmar-pagos',
        loadChildren: './mensaje-confirmar-pagos/mensaje-confirmar-pagos.module#MensajeConfirmarPagosPageModule'
      },
      {
        canActivate: [PagarGuard],
        path: 'mensaje-rechazar-pagos',
        loadChildren: './mensaje-rechazar-pagos/mensaje-rechazar-pagos.module#MensajeRechazarPagosPageModule'
      }
      ,
      {
        canActivate: [PagarGuard],
        path: 'pasarela',
        loadChildren: './pasarela/pasarela-recoger.module#PasarelaRecogerPageModule'
      },
      {
        canActivate: [PagarGuard],
        path: 'leer-qr-recoger',
        loadChildren: './leer-qr-recoger/leer-qr-recoger.module#LeerQrRecogerPageModule'
      }

    ]
  },

];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class PagosRoutingModule { }
