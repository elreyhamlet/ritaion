import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { RecogerPage } from './recoger.page';
import { RecogerRoutingModule } from './recoger-routing.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RecogerRoutingModule
  ],
  declarations: [RecogerPage]
})
export class RecogerPageModule { }
