import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { PedidoSesionService } from '../../../../sesion/pedido.sesion.service';
import { PedidoSocketService } from '../../../../services/pedido/pedido-socket.service';
import { MetodosNativosService } from '../../../../services/nativo/metodos-nativos.service';
import { Router, ActivatedRoute } from '@angular/router';
import { RecogerService } from '../../../../services/recoger/recoger.service';
import { CryptoService } from '../../../../services/crypto/crypto.service';

@Component({
  selector: 'app-qr-recoger',
  templateUrl: './qr-recoger.page.html',
  styleUrls: ['./qr-recoger.page.scss'],
})
export class QrRecogerPage implements OnInit, OnDestroy {


  value = '';
  cargando = false;
  mensaje = '';
  rita$: Subscription = new Subscription();
  qrCodigo = 0;
  constructor(
    private session: PedidoSesionService,
    private sPedido: PedidoSocketService,
    private nat: MetodosNativosService,
    private router: Router,
    private wsRecoger: RecogerService,
    private activated: ActivatedRoute,
    private crypto: CryptoService
  ) { }

  ngOnInit() {
    this.activated.params.subscribe((res: any) => {
      const pedidoId = res.id;
      this.preparar(pedidoId);
    });
  }

  ionViewWillEnter() {
    this.escucharRitaDisponible();
  }

  preparar(idPedido: number) {
    this.nat.obtenerLocalStorage('ID_TABLA').then((usuario: any) => {
      const pedidoId = idPedido;
      const clienteId = usuario;
      const cry = this.crypto.set(`${pedidoId} % ${clienteId}%`);
      this.value = encodeURIComponent(cry) + 'abc';
    }, () => {
      this.cargando = false;
      this.mensaje = '';
    });
  }
  escucharRitaDisponible() {
    this.rita$ = this.sPedido.escucharRitaEstado().
      subscribe((res: any) => {
        console.log(res);
        this.cargando = true;
        this.mensaje = 'Su pedido se está procesando';
        const tipo = res.payload.tipoMensaje * 1;
        switch (tipo) {
          case 1:
            this.router.navigate(['/pages/pag-int/recoger/recoger-cancelacion']);
            break;
          case 2:
            this.cargando = true;
            this.mensaje = res.payload.mensaje;
            break;
          case 3:
            this.router.navigate(['/pages/pag-int/recoger/recoger-confirmacion']);
            break;
          default:
            this.router.navigate(['/pages/pag-int/recoger/recoger-cancelacion']);
            break;
        }

      }, () => {
        this.cargando = false;
        this.router.navigate(['/pages/pag-int/recoger/recoger-cancelacion']);
        this.mensaje = '';
      }, () => {
        this.cargando = false;
        this.mensaje = '';

      });
  }

  ionViewWillLeave() {

    this.rita$.unsubscribe();
  }

  ngOnDestroy() {

    this.rita$.unsubscribe();
  }

  home() {
    this.router.navigate(['/pages/pag-int/home']);
  }
}
