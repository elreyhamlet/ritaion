import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { RecogerService } from '../../../../services/recoger/recoger.service';
import { PedidoSesionService } from '../../../../sesion/pedido.sesion.service';

@Component({
  selector: 'app-resumen-pedido-recoger',
  templateUrl: './resumen-pedido-recoger.page.html',
  styleUrls: ['./resumen-pedido-recoger.page.scss'],
})
export class ResumenPedidoRecogerPage implements OnInit {


  constructor(
    private router: Router,
    private params: ActivatedRoute,
    private service: RecogerService,
    private session: PedidoSesionService) { }
  montoTotal = 0.0;
  frutas: any = [];
  topping: any = null;
  pedidoId = 0;
  ngOnInit() {
    this.params.params.subscribe((res: any) => {
      const pedidoId = res.id;
      this.pedidoId = pedidoId;
      this.montoTotal = 0;
      let precio = 0.0;
      let bol = true;
      this.service.getListarJugos({ pedido_id: pedidoId }).subscribe((list: any) => {
        this.frutas = list.payload.frutas;
        this.topping = list.payload.topping[0];
        for (const item of this.frutas) {
          if (bol) {
            precio = item.fruta_precio * 1 + this.session.precioBase * 1;
            item.fruta_precio = precio;
          }
          this.montoTotal += item.fruta_precio * 1;
          bol = false;
        }
        if (this.topping) {
          this.montoTotal += this.topping.topping_precio * 1;
        }
      });
    });
  }


  regresar() {
    this.router.navigate(['/pages/pag-int/recoger']);
  }

  recoger() {
    this.router.navigate(['/pages/pag-int/recoger/qr-recoger', this.service.qrRecoger]);
  }
}
