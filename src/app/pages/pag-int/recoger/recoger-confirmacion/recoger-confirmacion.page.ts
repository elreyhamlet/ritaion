import { Component, OnInit, OnDestroy } from '@angular/core';
import { RecogerService } from '../../../../services/recoger/recoger.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-recoger-confirmacion',
  templateUrl: './recoger-confirmacion.page.html',
  styleUrls: ['./recoger-confirmacion.page.scss'],
})
export class RecogerConfirmacionPage implements OnInit, OnDestroy {

  constructor(private session: RecogerService, private router: Router) { }

  ngOnInit() {
    this.session.sessionRecoger = false;
  }
  ionViewWillEnter() {
    this.session.sessionRecoger = false;
  }

  ionViewWillLeave() {
    this.session.sessionRecoger = false;
  }

  ngOnDestroy() {
    this.session.sessionRecoger = false;
  }
  home() {

    this.router.navigate(['/pages/pag-int/home']);
  }
}
