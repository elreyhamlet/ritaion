import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { RegistrarPagoPosPage } from './registrar-pago-pos.page';

const routes: Routes = [
  {
    path: '',
    component: RegistrarPagoPosPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [RegistrarPagoPosPage]
})
export class RegistrarPagoPosPageModule {}
