import { Component, OnInit } from '@angular/core';
import { MetodosNativosService } from '../../../../services/nativo/metodos-nativos.service';

@Component({
  selector: 'app-recomendar',
  templateUrl: './recomendar.page.html',
  styleUrls: ['./recomendar.page.scss'],
})
export class RecomendarPage implements OnInit {

  constructor(private native: MetodosNativosService) { }

  ngOnInit() {
  }
  compartir(tipo: number) {

    switch (tipo) {
      case 1:
        this.native.compartir().shareViaFacebook('', '', 'https://www.disfruta.com.pe/');

        break;
      case 2:
        this.native.compartir().shareViaInstagram('https://www.disfruta.com.pe/', '');

        break;
      case 3:
        this.native.compartir().shareViaTwitter('Mensaje de pueba', '', 'https://www.disfruta.com.pe/');
        break;
      default:
        break;
    }
  }
}
