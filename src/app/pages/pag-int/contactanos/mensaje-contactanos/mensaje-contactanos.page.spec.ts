import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MensajeContactanosPage } from './mensaje-contactanos.page';

describe('MensajeContactanosPage', () => {
  let component: MensajeContactanosPage;
  let fixture: ComponentFixture<MensajeContactanosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MensajeContactanosPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MensajeContactanosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
