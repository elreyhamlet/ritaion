import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PublicService } from '../../../../services/public/public.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-buzon',
  templateUrl: './buzon.page.html',
  styleUrls: ['./buzon.page.scss'],
})
export class BuzonPage implements OnInit {

  formulario: FormGroup;
  constructor(
    private ws: PublicService,
    private routes: Router,
    private mat: MatSnackBar
  ) { }
  lista = [];
  ngOnInit() {
    this.formulario = new FormGroup({
      nombres: new FormControl('', Validators.required),
      tipoConsulta: new FormControl('', Validators.required),
      ciudad: new FormControl('', Validators.required),
      celular: new FormControl('', Validators.required),
      descripcion: new FormControl('', Validators.required)
    });

    this.listarTipoConsulta();
  }
  listarTipoConsulta() {
    this.ws.listarTipoConsulta().subscribe((res: any) => {
      this.lista = res.payload;
    });
  }
  registrarConsulta() {
    this.ws.registrarConsulta(this.formulario.value).subscribe((res: any) => {
      if (res.estado) {
        this.routes.navigate(['/pages/pag-int/contactanos/mensaje-contactanos']);
      } else {
        this.mat.open(res.mensaje, 'Cerrar', { duration: 3000 });
      }
    });

  }


}
