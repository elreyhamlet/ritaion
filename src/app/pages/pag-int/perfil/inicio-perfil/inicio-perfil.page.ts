import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UbigeoService } from '../../../../services/ubigeo/ubigeo.service';
import { AccionService } from '../../../../services/accion/accion.service';
import { UsuarioService } from '../../../../services/usuario/usuario.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MetodosService } from '../../../../services/metodos/metodos.service';
import { Router } from '@angular/router';
import { DURATIONSNACK } from 'src/app/config/variables';
import { MetodosNativosService } from '../../../../services/nativo/metodos-nativos.service';

@Component({
  selector: 'app-inicio-perfil',
  templateUrl: './inicio-perfil.page.html',
  styleUrls: ['./inicio-perfil.page.scss'],
})
export class InicioPerfilPage implements OnInit {

  private pattern = '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$';
  public formulario: FormGroup;
  public persona = {
    nombre: '',
    apellido: '',
    telefono: '',
    correo: ''
  };


  constructor(
    private usuario: UsuarioService,
    private snackBar: MatSnackBar,
    public mensaje: MetodosService,
    public router: Router,
    private nat: MetodosNativosService) {

  }

  ngOnInit() {
    this.formulario = new FormGroup({
      'persona_nombre': new FormControl('', Validators.required),
      'persona_apellido': new FormControl('', Validators.required),
      'persona_telefono': new FormControl('', Validators.required),
      'persona_correo': new FormControl('', [Validators.required, Validators.pattern(this.pattern)])
    });
    this.getDatosUsuario();

  }

  getDatosUsuario() {
    this.nat.obtenerLocalStorage('usuario').then((item: any) => {
      console.log(item);
      const id = item.usuario_id;
      this.usuario.getDatosUsuario(JSON.stringify({ usuario_id: id }))
        .subscribe((data: any) => {

          this.formulario.get('persona_nombre').setValue(data.payload.persona_nombre);
          this.formulario.get('persona_apellido').setValue(data.payload.persona_apellido);
          this.formulario.get('persona_telefono').setValue(data.payload.persona_telefono);
          this.formulario.get('persona_correo').setValue(data.payload.persona_correo);

        });
    }, (res1) => {
      console.log(res1);
    });
  }

  edit() {

    if (this.validarFormulario()) {
      this.usuario.getUpdateUsuario(JSON.stringify(this.formulario.value)).subscribe((res: any) => {
        console.log(res);
        if (res.estado) {
          this.router.navigate(['/pages/pag-int/perfil/mensaje-confirmar-perfil']);
        } else {
          this.snackBar.open('No se puedo actualizar los datos', 'Cerrar', { duration: 3000 });
        }
      });
    }

  }


  validarFormulario() {

    if (this.formulario.controls['persona_nombre'].invalid) {
      this.snackBar.open('Ingrese nombre', 'cerrar', { duration: DURATIONSNACK });
      return false;
    }
    else if (this.formulario.controls['persona_apellido'].invalid) {
      this.snackBar.open('Ingrese apellido', 'cerrar', { duration: DURATIONSNACK });
      return false;
    }
    else if (this.formulario.controls['persona_telefono'].invalid) {
      this.snackBar.open('Ingrese teléfono', 'cerrar', { duration: DURATIONSNACK });
      return false;
    }
    else if (this.formulario.controls['persona_correo'].invalid) {
      this.snackBar.open('Correo inválido', 'cerrar', { duration: DURATIONSNACK });
      return false;
    }
    else {

      return true;
    }

  }

  home() {
    this.router.navigate(['/home']);
  }
}
