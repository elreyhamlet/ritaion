import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PagExtPage } from './pag-ext.page';


const routes: Routes = [
  {
    path: '', component: PagExtPage, children: [
      { path: '', redirectTo: 'login' },
      { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
      { path: 'menu-publico', loadChildren: './menu-publico/menu-publico.module#MenuPublicoPageModule' }

    ]
  }

];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class PagesExtRoutingModule { }
