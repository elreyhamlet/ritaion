import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { RecuperarPasswordPage } from './recuperar-password.page';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { SharedModule } from '../../../../shared/shared.module';


const routes: Routes = [
  {
    path: '',
    component: RecuperarPasswordPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    MatSnackBarModule,
    SharedModule
  ],
  declarations: [RecuperarPasswordPage]
})
export class RecuperarPasswordPageModule { }
