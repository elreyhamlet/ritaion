import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-mensaje-confirmacion-login',
  templateUrl: './mensaje-confirmacion-login.page.html',
  styleUrls: ['./mensaje-confirmacion-login.page.scss'],
})
export class MensajeConfirmacionLoginPage implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  view() {
    this.router.navigate(['/login']);
  }
}
