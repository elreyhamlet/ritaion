import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContactanosPublicPage } from './contactanos-public.page';

const routes: Routes = [
  {
    path: '', component: ContactanosPublicPage, children: [
      { path: '', redirectTo: 'inicio' },
      { path: 'buzon-public', loadChildren: './buzon-public/buzon-public.module#BuzonPublicPageModule' },
      { path: 'inicio', loadChildren: './inicio/inicio.module#InicioPageModule' },
      { path: 'recomendar-public', loadChildren: './recomendar-public/recomendar-public.module#RecomendarPublicPageModule' },
      { path: 'mensaje-public', loadChildren: './mensaje-public/mensaje-public.module#MensajePublicPageModule' }

    ]
  },

];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class ContactanosPublicoRoutingModule { }
