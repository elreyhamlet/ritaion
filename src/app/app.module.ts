import { LoadingScreenComponent } from './loading-screen/loading-screen.component';
import { LoadingScreenInterceptor } from './helpers/loading.interceptor';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import localeFr from '@angular/common/locales/es-PE';
import { registerLocaleData } from '@angular/common';
import { NativeModule } from './native.module';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import { URL_SERVER_SOCKETS } from './config/config';
import { AgmCoreModule } from '@agm/core';
import { API_KEY } from './config/variables';
import { NgxQRCodeModule } from 'ngx-qrcode2';
import { MatSnackBarModule } from '@angular/material/snack-bar';

const config: SocketIoConfig = { url: URL_SERVER_SOCKETS, options: {} };
// the second parameter 'fr' is optional
registerLocaleData(localeFr, 'PE');
@NgModule({
  declarations: [AppComponent, LoadingScreenComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    NativeModule,
    SocketIoModule.forRoot(config),
    AgmCoreModule.forRoot({
      apiKey: API_KEY
    }),
    NgxQRCodeModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoadingScreenInterceptor,
      multi: true
    },
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
